# Changelog

All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2021-05-17

- Unity 2020.2 support (`UnityEditor.Experimental.AssetImporters` -> `UnityEditor.AssetImporters`)

## [1.0.0] - 2020-06-28

BSD 2-Clause License and better Runtime/Editor separation.

## [0.0.1-preview.1] - 2018-06-23

This is the first release.
