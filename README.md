# Mosaic Package for Unity3D

Imports mosaic assets into _Unity3D_ compatible representations.

## License

BSD 2-Clause License (see [LICENSE](LICENSE.md))