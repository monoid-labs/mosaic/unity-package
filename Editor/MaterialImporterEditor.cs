using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Mosaic {

  [CustomEditor(typeof(MaterialImporter))]
  public class MaterialImporterEditor : ScriptedImporterEditor {

    public override void OnInspectorGUI() {
      var importer = (MaterialImporter)target;
      var material = AssetDatabase.LoadAssetAtPath<UnityEngine.Material>(importer.assetPath);
      importer.serialized = EditorJsonUtility.ToJson(material);
      EditorUtility.SetDirty(importer);
      AssetDatabase.WriteImportSettingsIfDirty(importer.assetPath);
    }
  }

}
