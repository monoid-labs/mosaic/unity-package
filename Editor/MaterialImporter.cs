using System;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif


namespace Monoid.Unity.Mosaic {

  [ScriptedImporter(1, ".mosaic-material", 21)]
  public class MaterialImporter : ScriptedImporter {

    static MaterialImporter() {
      ShaderProperties.Set("Standard", new ShaderProperties.Standard());
    }


    public string serialized = string.Empty;

    public override void OnImportAsset(AssetImportContext ctx) {

      var path = ctx.assetPath;
      var dir = Path.GetDirectoryName(path);
      var name = Path.GetFileNameWithoutExtension(path);

      string materialJson = null;
      using (var archive = ZipFile.OpenRead(path)) {
        foreach (var entry in archive.Entries) {
          switch (entry.Name.ToLower()) {
            case "material.json":
              materialJson = Utils.Zip.ReadText(entry);
              break;
            default:
              Debug.LogWarning("Mosaic Material: unsupported piece: " + entry.Name);
              break;
          }
        }
      }

      var material = AssetDatabase.LoadAssetAtPath<UnityEngine.Material>(path);
      if (material && !string.IsNullOrEmpty(serialized)) {
        EditorJsonUtility.FromJsonOverwrite(serialized, material);
      }
      if (!material) {
        var shader = UnityEngine.Shader.Find("Standard");
        material = new UnityEngine.Material(shader);
      }
      material.name = name;

      if (!LoadMaterial(material, materialJson, dir)) {
        Debug.LogError("Mosaic Material: could not load material: " + name);
        return;
      }

      ctx.AddObjectToAsset(name, material);
      ctx.SetMainObject(material);

      serialized = EditorJsonUtility.ToJson(material);
    }

    bool LoadMaterial(UnityEngine.Material material, string json, string path) {
      if (json == null) {
        return false;
      }
      var mat = JsonUtility.FromJson<Material>(json);
      if (mat == null) {
        return false;
      }
      return Load(mat, material, path);
    }


    UnityEngine.Material[] LoadMaterials(string[] paths) {
      var materials = new UnityEngine.Material[paths.Length];
      for (int i = 0; i < materials.Length; i++) {
        if (string.IsNullOrEmpty(paths[i])) {
          continue;
        }
        materials[i] = ImportMaterial(paths[i]);
      }
      return materials;
    }

    public static UnityEngine.Material ImportMaterial(string asset) {
      var name = Path.GetFileName(asset);
      var assetPath = asset + ".mat";

      var material = AssetDatabase.LoadAssetAtPath<UnityEngine.Material>(asset + ".mosaic-material");
      if (material) {
        return material;
      }

      material = AssetDatabase.LoadAssetAtPath<UnityEngine.Material>(assetPath);
      if (!material) {
        material = new UnityEngine.Material(UnityEngine.Shader.Find("Standard"));
        AssetDatabase.CreateAsset(material, assetPath);
      }

      material.name = name;

      // albedo map
      var albedoMap = Utils.Assets.LoadTexture2D(asset);
      if (!albedoMap) {
        albedoMap = Utils.Assets.LoadTexture2D(asset + "_albedo");
      }
      if (albedoMap) {
        material.mainTexture = albedoMap;
      }
      // normal map
      var normalMap = Utils.Assets.LoadTexture2D(asset + "_normal");
      if (normalMap) {
        material.SetTexture("_BumpMap", normalMap);
      }
      // emission map
      var emissionMap = Utils.Assets.LoadTexture2D(asset + "_emission");
      if (emissionMap) {
        material.EnableKeyword("_EMISSION");
        material.SetTexture("_EmissionMap", emissionMap);
      }
      // metal/roughness map
      var metalMap = Utils.Assets.LoadTexture2D(asset + "_metal");
      if (!metalMap) {
        metalMap = Utils.Assets.LoadTexture2D(asset + "_metalness");
      }
      if (!metalMap) {
        metalMap = Utils.Assets.LoadTexture2D(asset + "_metallic");
      }
      if (metalMap) {
        material.EnableKeyword("_METALLICGLOSSMAP");
        material.SetTexture("_MetallicGlossMap", metalMap);
      }

      return material;
    }

    static bool Load(Material material, UnityEngine.Material mat, string path) {
      if (string.IsNullOrEmpty(material.shader)) {
        material.shader = "Standard";
      }

      if (!mat.shader || mat.shader.name.ToLower() != material.shader.ToLower()) {
        var shaderProgram = UnityEngine.Shader.Find(material.shader);
        if (!shaderProgram) {
          Debug.LogWarning("Mosaic Material: Shader not found: " + material.shader);
          return true;
        }
        mat.shader = shaderProgram;
      }

      var properties = ShaderProperties.Get(mat.shader.name);
      if (properties == null) {
        Debug.LogWarning("Mosaic Material: No shader property map found for: " + mat.shader.name);
        return true;
      }

      for (int i = 0; i < material.textures.Length; i++) {
        var texture = material.textures[i];
        if (string.IsNullOrEmpty(texture.name)) {
          Debug.LogWarning("Mosaic Material: Texture[" + i + "] has no name");
          continue;
        }
        if (string.IsNullOrEmpty(texture.image)) {
          Debug.LogWarning("Mosaic Material: Texture[" + i + "](" + texture.name + ") has no image");
          continue;
        }

        var file = Utils.Path.Resolve(texture.image, path);
        UnityEngine.Texture tex;
        tex = AssetDatabase.LoadAssetAtPath<UnityEngine.Texture>(file);
        if (!tex) {
          Debug.LogWarning("Mosaic Material: Texture[" + i + "](" + texture.name + ") image was not found at: " + file);
          continue;
        }

        properties.Set(mat, texture.name, tex);
      }

      for (int i = 0; i < material.constants.Length; i++) {
        var constant = material.constants[i];
        if (string.IsNullOrEmpty(constant.name)) {
          Debug.LogWarning("Mosaic Material: Constant[" + i + "] has no name");
          continue;
        }
        if (constant.value == null) {
          constant.value = new float[0];
        }
        Array.Resize(ref constant.value, Math.Min(constant.value.Length, 4));

        var c = Vector4.zero;
        for (int j = 0; j < constant.value.Length; j++) {
          c[j] = constant.value[j];
        }

        properties.Set(mat, constant.name, c);
      }

      return true;
    }
  }

}
