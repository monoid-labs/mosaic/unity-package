using UnityEditor;
using UnityEngine;

namespace Monoid.Unity.Mosaic.Utils {

  public static partial class Assets {

    public static void Copy(UnityEngine.Mesh src, UnityEngine.Mesh dst) {
      dst.Clear();
      var combine = new CombineInstance[1];
      combine[0].mesh = src;
      combine[0].transform = Matrix4x4.identity;
      dst.CombineMeshes(combine, false, false);
    }

    public static Texture2D LoadTexture2D(string path) {
      Texture2D texture;
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".tga");
      if (texture) {
        return texture;
      }
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".png");
      if (texture) {
        return texture;
      }
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".jpg");
      if (texture) {
        return texture;
      }
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".jpeg");
      return texture;
    }

  }
}
