using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Mosaic.Utils {

  public sealed class AnimationClipProperties {

    // float startTime;
    // float stopTime;
    // float orientationOffsetY;
    // float level;
    public float cycleOffset;

    public bool loopTime;

    public bool loopBlend;
    // bool loopBlendOrientation;
    // bool loopBlendPositionY;
    // bool loopBlendPositionXZ;
    // bool keepOriginalOrientation;
    // bool keepOriginalPositionY;
    // bool keepOriginalPositionXZ;
    // bool heightFromFeet;
    // bool mirror;

    public void Load(AnimationClip clip) {
      var serializedClip = new SerializedObject(clip);
      var property = serializedClip.FindProperty("m_AnimationClipSettings");

      // startTime = Get(property, "m_StartTime").floatValue;
      // stopTime = Get(property, "m_StopTime").floatValue;
      // orientationOffsetY = Get(property, "m_OrientationOffsetY").floatValue;
      // level = Get(property, "m_Level").floatValue;
      cycleOffset = Get(property, "m_CycleOffset").floatValue;

      loopTime = Get(property, "m_LoopTime").boolValue;

      loopBlend = Get(property, "m_LoopBlend").boolValue;
      // loopBlendOrientation = Get(property, "m_LoopBlendOrientation").boolValue;
      // loopBlendPositionY = Get(property, "m_LoopBlendPositionY").boolValue;
      // loopBlendPositionXZ = Get(property, "m_LoopBlendPositionXZ").boolValue;
      // keepOriginalOrientation = Get(property, "m_KeepOriginalOrientation").boolValue;
      // keepOriginalPositionY = Get(property, "m_KeepOriginalPositionY").boolValue;
      // keepOriginalPositionXZ = Get(property, "m_KeepOriginalPositionXZ").boolValue;
      // heightFromFeet = Get(property, "m_HeightFromFeet").boolValue;
      // mirror = Get(property, "m_Mirror").boolValue;
    }

    public void Save(AnimationClip clip) {
      var serializedClip = new SerializedObject(clip);
      var property = serializedClip.FindProperty("m_AnimationClipSettings");

      // Get(property, "m_StartTime").floatValue = startTime;
      // Get(property, "m_StopTime").floatValue = stopTime;
      // Get(property, "m_OrientationOffsetY").floatValue = orientationOffsetY;
      // Get(property, "m_Level").floatValue = level;
      Get(property, "m_CycleOffset").floatValue = cycleOffset;

      Get(property, "m_LoopTime").boolValue = loopTime;

      Get(property, "m_LoopBlend").boolValue = loopBlend;
      // Get(property, "m_LoopBlendOrientation").boolValue = loopBlendOrientation;
      // Get(property, "m_LoopBlendPositionY").boolValue = loopBlendPositionY;
      // Get(property, "m_LoopBlendPositionXZ").boolValue = loopBlendPositionXZ;
      // Get(property, "m_KeepOriginalOrientation").boolValue = keepOriginalOrientation;
      // Get(property, "m_KeepOriginalPositionY").boolValue = keepOriginalPositionY;
      // Get(property, "m_KeepOriginalPositionXZ").boolValue = keepOriginalPositionXZ;
      // Get(property, "m_HeightFromFeet").boolValue = heightFromFeet;
      // Get(property, "m_Mirror").boolValue = mirror;

      serializedClip.ApplyModifiedProperties();
    }

    static SerializedProperty Get(SerializedProperty property, string name) {
      return property.FindPropertyRelative(name);
    }
  }

}
