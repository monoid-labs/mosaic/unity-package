using System;
using System.Linq;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Mosaic {

  // https://forum.unity.com/threads/scripted-importers-identifier-uniqueness-violation.518589/

  [ScriptedImporter(1, ".mosaic-model", 25)]
  public class ModelImporter : ScriptedImporter {

    [Serializable]
    public struct AnimationClipSettings : IEquatable<AnimationClipSettings> {
      public string name;
      public bool loopTime, loopBlend;
      public float cycleOffset;

      #region IEquatable<AnimationClipSettings>

      public override int GetHashCode() => name?.GetHashCode() ?? 0;

      public override bool Equals(object other) {
        return other is AnimationClipSettings && Equals((AnimationClipSettings)other);
      }

      public bool Equals(AnimationClipSettings other) {
        return name == other.name && loopTime == other.loopTime && loopBlend == other.loopBlend && cycleOffset == other.cycleOffset;
      }

      public static bool operator ==(AnimationClipSettings a, AnimationClipSettings b) => a.Equals(b);

      public static bool operator !=(AnimationClipSettings a, AnimationClipSettings b) => !a.Equals(b);

      #endregion

      public void Load(UnityEngine.AnimationClip clip) {
        var settings = new Utils.AnimationClipProperties();
        settings.Load(clip);
        name = clip.name;
        loopTime = settings.loopTime;
        loopBlend = settings.loopBlend;
        cycleOffset = settings.cycleOffset;
      }

      public void Save(UnityEngine.AnimationClip clip) {
        var settings = new Utils.AnimationClipProperties();
        settings.Load(clip);
        settings.loopTime = loopTime;
        settings.loopBlend = loopBlend;
        settings.cycleOffset = cycleOffset;
        settings.Save(clip);
      }

      public static void Overwrite(UnityEngine.AnimationClip[] clips, AnimationClipSettings[] settings) {
        if (settings.Length <= 0) {
          return;
        }
        var clipMap = clips.ToDictionary(clip => clip.name, clip => clip);
        for (int i = 0, n = settings.Length; i < n; i++) {
          UnityEngine.AnimationClip clip;
          if (clipMap.TryGetValue(settings[i].name, out clip)) {
            settings[i].Save(clip);
          }
        }
      }

      public static bool Extract(UnityEngine.AnimationClip[] clips, ref AnimationClipSettings[] settings) {
        var dirty = settings.Length != clips.Length;
        Array.Resize(ref settings, clips.Length);
        for (int i = 0, n = settings.Length; i < n; i++) {
          var old = settings[i];
          settings[i].Load(clips[i]);
          dirty |= old != settings[i];
        }
        return dirty;
      }

    }

    public bool addAnimator = true;
    public bool addAvatar = true;
    public bool optimizeGameObjects = false;

    public bool importAnimations = true;
    public AnimationClipSettings[] animClipSettings = { };

    public override void OnImportAsset(AssetImportContext ctx) {

      var path = ctx.assetPath;
      var dir = Path.GetDirectoryName(path);
      var name = Path.GetFileNameWithoutExtension(path);

      string modelJson = null;

      string animJson = null;
      byte[] animBytes = null;

      using (var archive = ZipFile.OpenRead(path)) {
        foreach (var entry in archive.Entries) {
          switch (entry.Name.ToLower()) {
            case "model.json":
              modelJson = Utils.Zip.ReadText(entry);
              break;
            case "anim.json":
              animJson = Utils.Zip.ReadText(entry);
              break;
            case "anim.bytes":
              animBytes = Utils.Zip.ReadBinary(entry);
              break;
            default:
              Debug.LogWarning("Mosaic Model: unsupported piece: " + entry.Name);
              break;
          }
        }
      }

      var prefab = new GameObject();
      prefab.name = name;

      UnityEngine.Transform[] bones;
      if (!LoadModel(prefab, out bones, modelJson, dir)) {
        Debug.LogError("Mosaic Model: could not load model: " + name);
        return;
      }

      UnityEngine.AnimationClip[] clips = { };
      if (importAnimations) {
        // AnimatorUtility.DeoptimizeTransformHierarchy(prefab);
        if (!LoadAnimations(prefab, out clips, animJson, animBytes, name)) {
          Debug.LogError("Mosaic Model: could not load model: " + name);
          return;
        }
      }


      AnimationClipSettings.Overwrite(clips, animClipSettings);

      ctx.AddObjectToAsset(name, prefab);
      ctx.SetMainObject(prefab);
      if (clips.Length > 0) {
        var animator = addAnimator ? prefab.AddComponent<Animator>() : null;

        if (addAvatar) {
          var avatar = UnityEngine.AvatarBuilder.BuildGenericAvatar(prefab, "");
          avatar.name = name;
          ctx.AddObjectToAsset(name, avatar);
          if (animator) {
            animator.avatar = avatar;
          }
        }
        foreach (var clip in clips) {
          ctx.AddObjectToAsset(clip.name, clip);
        }

        if (animator && optimizeGameObjects) {
          AnimatorUtility.OptimizeTransformHierarchy(prefab, new string[] { });
        }
      }

      if (AnimationClipSettings.Extract(clips, ref animClipSettings)) {
        EditorUtility.SetDirty(this);
      }
      AssetDatabase.WriteImportSettingsIfDirty(path);
    }
    bool LoadModel(GameObject prefab, out UnityEngine.Transform[] bones, string json, string path) {
      bones = new UnityEngine.Transform[0];
      if (json == null) {
        return false;
      }
      var model = JsonUtility.FromJson<Model>(json);
      if (model == null) {
        return false;
      }

      prefab.transform.localPosition = Vector3.zero;
      prefab.transform.localRotation = Quaternion.identity;
      prefab.transform.localScale = Vector3.one;

      bones = new UnityEngine.Transform[model.bones.Length];
      for (int i = 0; i < bones.Length; i++) {
        var boneInfo = model.bones[i];
        var bone = bones[i] = new GameObject(boneInfo.name).transform;
        boneInfo.transform.Set(bone);

        // var cube =  prefab.CreatePrimitive(PrimitiveType.Cube);
        // cube.name = "cube";
        // cube.transform.parent = bone.transform;
        // cube.transform.localPosition = Vector3.zero;
        // cube.transform.localRotation = Quaternion.identity;
        // cube.transform.localScale = Vector3.one * 0.025f;
      }

      // second pass to allow for order where children are before parents
      for (int i = 0; i < bones.Length; i++) {
        var bone = bones[i];
        var boneInfo = model.bones[i];
        bone.SetParent(boneInfo.parent > 0 ? bones[boneInfo.parent - 1] : prefab.transform, false);
      }

      foreach (var shape in model.shapes) {
        if (string.IsNullOrEmpty(shape.name)) {
          continue;
        }

        string name, shapePath;
        Names.Instance(shape.name, out name, out shapePath);
        shapePath = Utils.Path.Resolve(shapePath, path);

        UnityEngine.Mesh mesh = null;
        var materials = new UnityEngine.Material[0];

        var meshPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(shapePath + ".mosaic-mesh");
        if (!meshPrefab) {
          meshPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(shapePath + ".prefab");
        }
        if (meshPrefab) {
          var skinnedRenderer = meshPrefab.GetComponent<SkinnedMeshRenderer>();
          if (skinnedRenderer) {
            mesh = skinnedRenderer.sharedMesh;
            materials = skinnedRenderer.sharedMaterials;
          }

          var filter = meshPrefab.GetComponent<MeshFilter>();
          if (filter) {
            mesh = filter.sharedMesh;
          }
          var renderer = meshPrefab.GetComponent<MeshRenderer>();
          if (renderer) {
            materials = renderer.sharedMaterials;
          }
        }
        if (!mesh) {
          Debug.LogWarning("Mosaic-Model: Mesh not found: " + path);
          continue;
        }

        UnityEngine.Transform child;
        if (shape.bone > 0) {
          child = bones[shape.bone - 1];
        } else {
          child = prefab.transform.Find(name);
          if (!child) {
            child = new GameObject(name).transform;
            child.parent = prefab.transform;
          }
          child.localPosition = Vector3.zero;
          child.localRotation = Quaternion.identity;
          child.localScale = Vector3.one;
        }

        var hasSkin = (mesh.boneWeights.Length > 0 && mesh.bindposes.Length > 0 && mesh.bindposes.Length <= bones.Length) || mesh.blendShapeCount > 0;

        if (hasSkin) {
          var renderer = child.gameObject.AddComponent<SkinnedMeshRenderer>();
          renderer.bones = bones;
          renderer.sharedMesh = mesh;
          renderer.sharedMaterials = materials;
        } else {
          var filter = child.gameObject.AddComponent<MeshFilter>();
          filter.sharedMesh = mesh;
          var renderer = child.gameObject.AddComponent<MeshRenderer>();
          renderer.sharedMaterials = materials;
        }
      }
      return true;
    }

    bool LoadAnimations(GameObject prefab, out UnityEngine.AnimationClip[] clips, string json, byte[] bytes, string name) {
      clips = new UnityEngine.AnimationClip[0];
      if (json == null && bytes == null) {
        return true;
      }
      if (json == null || bytes == null) {
        return false;
      }
      var anim = JsonUtility.FromJson<Animations>(json);
      if (anim == null) {
        return false;
      }
      clips = anim.LoadActions(name, bytes, prefab.transform);
      return true;
    }
  }

}
