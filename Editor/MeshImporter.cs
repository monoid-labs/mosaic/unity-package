using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Mosaic {

  [ScriptedImporter(1, ".mosaic-mesh", 23)]
  public class MeshImporter : ScriptedImporter {

    public Generate generateNormals = Generate.FillIn;
    public Generate generateTangents = Generate.FillIn;

    public bool importSkinWeights = true;
    public bool importBlendShapes = true;
    public bool useSkinnedMeshRenderer = false;

    public bool isReadable = true;
    //public bool markDynamic = false;

    public override void OnImportAsset(AssetImportContext ctx) {

      var path = ctx.assetPath;
      var dir = Path.GetDirectoryName(path);
      var name = Path.GetFileNameWithoutExtension(path);

      string meshJson = null;
      byte[] meshBytes = null;

      string morphJson = null;
      byte[] morphBytes = null;

      string skinJson = null;
      byte[] skinBytes = null;

      using (var archive = ZipFile.OpenRead(path)) {
        foreach (var entry in archive.Entries) {
          switch (entry.Name.ToLower()) {
            case "mesh.json":
              meshJson = Utils.Zip.ReadText(entry);
              break;
            case "mesh.bytes":
              meshBytes = Utils.Zip.ReadBinary(entry);
              break;
            case "morph.json":
              morphJson = Utils.Zip.ReadText(entry);
              break;
            case "morph.bytes":
              morphBytes = Utils.Zip.ReadBinary(entry);
              break;
            case "skin.json":
              skinJson = Utils.Zip.ReadText(entry);
              break;
            case "skin.bytes":
              skinBytes = Utils.Zip.ReadBinary(entry);
              break;
            default:
              Debug.LogWarning("Mosaic Mesh: unsupported piece: " + entry.Name);
              break;
          }
        }
      }

      var mesh = new UnityEngine.Mesh();
      mesh.name = name;

      string[] materials;
      if (!LoadMesh(mesh, out materials, meshJson, meshBytes, dir)) {
        Debug.LogError("Mosaic Mesh: could not load mesh: " + name);
        return;
      }

      string[] states = { };
      if (importBlendShapes) {
        if (!LoadMorph(mesh, out states, morphJson, morphBytes)) {
          Debug.LogError("Mosaic Mesh: could not load blend shapes: " + name);
        }
      }
      string[] bones = { };
      if (importSkinWeights) {
        if (!LoadSkin(mesh, out bones, skinJson, skinBytes)) {
          Debug.LogError("Mosaic Mesh: could not load skins: " + name);
        }
      }

      mesh.RecalculateBounds();

      if (!isReadable) {
        mesh.UploadMeshData(true);
      }

      var prefab = new GameObject(name);
      if (useSkinnedMeshRenderer && (states.Length + bones.Length > 0)) {
        var renderer = prefab.GetComponent<SkinnedMeshRenderer>();
        if (!renderer) {
          renderer = prefab.AddComponent<SkinnedMeshRenderer>();
        }
        renderer.sharedMesh = mesh;
        renderer.sharedMaterials = LoadMaterials(materials);
      } else {
        var filter = prefab.GetComponent<MeshFilter>();
        if (!filter) {
          filter = prefab.AddComponent<MeshFilter>();
        }
        var renderer = prefab.GetComponent<MeshRenderer>();
        if (!renderer) {
          renderer = prefab.AddComponent<MeshRenderer>();
        }
        filter.sharedMesh = mesh;
        renderer.sharedMaterials = LoadMaterials(materials);
      }

      ctx.AddObjectToAsset(name, prefab);
      ctx.AddObjectToAsset(name, mesh);
      ctx.SetMainObject(prefab);
    }

    bool LoadMesh(UnityEngine.Mesh mesh, out string[] materials, string json, byte[] bytes, string path) {
      materials = new string[0];
      if (json == null || bytes == null) {
        return false;
      }
      var _mesh = JsonUtility.FromJson<Mesh>(json);
      if (_mesh == null) {
        return false;
      }
      materials = _mesh.MaterialPaths(path);
      return _mesh.Load(bytes, mesh, generateNormals, generateTangents);
    }

    bool LoadMorph(UnityEngine.Mesh mesh, out string[] states, string json, byte[] bytes) {
      states = new string[0];
      if (json == null && bytes == null) {
        return true;
      }
      if (json == null || bytes == null) {
        return false;
      }
      var modifier = JsonUtility.FromJson<Modifier>(json);
      if (modifier == null) {
        return false;
      }
      states = modifier.Names();
      return modifier.LoadMorph(bytes, mesh, generateNormals, generateTangents);
    }

    bool LoadSkin(UnityEngine.Mesh mesh, out string[] bones, string json, byte[] bytes) {
      bones = new string[0];
      if (json == null && bytes == null) {
        return true;
      }
      if (json == null || bytes == null) {
        return false;
      }
      var modifier = JsonUtility.FromJson<Modifier>(json);
      if (modifier == null) {
        return false;
      }
      bones = modifier.Names();
      return modifier.LoadSkin(bytes, mesh);
    }

    UnityEngine.Material[] LoadMaterials(string[] paths) {
      var materials = new UnityEngine.Material[paths.Length];
      for (int i = 0; i < materials.Length; i++) {
        if (string.IsNullOrEmpty(paths[i])) {
          continue;
        }
        materials[i] = MaterialImporter.ImportMaterial(paths[i]);
        //AssetDatabase.LoadAssetAtPath<UnityEngine.Material>(paths[i] + ".mat");
      }
      return materials;
    }

    static Texture2D LoadTexture(string path) {
      Texture2D texture;
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".tga");
      if (texture) {
        return texture;
      }
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".png");
      if (texture) {
        return texture;
      }
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".jpg");
      if (texture) {
        return texture;
      }
      texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path + ".jpeg");
      return texture;
    }
  }

}
