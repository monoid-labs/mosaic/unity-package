using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Mosaic {

  [CustomEditor(typeof(MeshImporter))]
  public class MeshImporterEditor : ScriptedImporterEditor {

    SerializedProperty generateNormals, generateTangents;
    SerializedProperty importBlendShapes, importSkinWeights;
    SerializedProperty useSkinnedMeshRenderer;
    SerializedProperty isReadable; //, markDynamic

    static class Styles {
      public static readonly GUIContent Title = new GUIContent("Mesh", "These options control how the mesh is imported.");

      public static readonly GUIContent GenerateNormalsLabel = new GUIContent("Generate Normals");
      public static readonly GUIContent GenerateTangentsLabel = new GUIContent("Generate Tangents");
      public static readonly GUIContent[] GenerateLabelOpt = {
              new GUIContent("Clear", "Don't import and don't generate."),
              new GUIContent("Import", "Import if present."),
              new GUIContent("Fill-In ", "Import if present, otherwise generate."),
              new GUIContent("Replace", "Always generate and override if imported."),
          };

      public static readonly GUIContent ImportBlendShapes = new GUIContent("Import BlendShapes", "Should Unity import BlendShapes.");
      public static readonly GUIContent ImportSkinWeights = new GUIContent("Import SkinWeights", "Should Unity import SkinWeights.");
      public static readonly GUIContent UseSkinnedMeshRenderer = new GUIContent("Use SkinnedMeshRenderer", "Will create a SkinnedMeshRenderer.\nDefault are a MeshFilter and MeshRenderer.");

      public static readonly GUIContent IsReadable = new GUIContent("Read/Write Enabled", "Allow vertices and indices to be accessed from script.");

      // ------------------------------------------------------------------
      // NOTE(micha): maybe in the future
      // ------------------------------------------------------------------

      // public GUIContent IndexFormatLabel = new GUIContent("Index Format", "Format of mesh index buffer. Auto mode picks 16 or 32 bit depending on mesh vertex count.");
      // public GUIContent[] IndexFormatOpt = { new GUIContent("Auto"), new GUIContent("Force 32 bit") };

      // public GUIContent KeepQuads = new GUIContent("Keep Quads", "If model contains quad faces, they are kept for DX11 tessellation.");
      // public GUIContent ImportVisibility = new GUIContent("Import Visibility", "Use visibility properties to enable or disable MeshRenderer components.");

      // public GUIContent GenerateColliders = new GUIContent("Generate Colliders", "Should Unity generate mesh colliders for all meshes.");
    }

    public override void OnEnable() {
      base.OnEnable();

      generateNormals = serializedObject.FindProperty("generateNormals");
      generateTangents = serializedObject.FindProperty("generateTangents");
      importBlendShapes = serializedObject.FindProperty("importBlendShapes");
      importSkinWeights = serializedObject.FindProperty("importSkinWeights");
      useSkinnedMeshRenderer = serializedObject.FindProperty("useSkinnedMeshRenderer");
      isReadable = serializedObject.FindProperty("isReadable");
    }

    public override void OnInspectorGUI() {
      GUILayout.Label(Styles.Title, EditorStyles.boldLabel);
      EditorGUILayout.PropertyField(importBlendShapes, Styles.ImportBlendShapes);
      EditorGUILayout.PropertyField(importSkinWeights, Styles.ImportSkinWeights);
      EditorGUILayout.PropertyField(useSkinnedMeshRenderer, Styles.UseSkinnedMeshRenderer);
      EditorGUILayout.PropertyField(isReadable, Styles.IsReadable);

      generateNormals.intValue = EditorGUILayout.Popup(Styles.GenerateNormalsLabel, generateNormals.intValue, Styles.GenerateLabelOpt);
      generateTangents.intValue = EditorGUILayout.Popup(Styles.GenerateTangentsLabel, generateTangents.intValue, Styles.GenerateLabelOpt);

      ApplyRevertGUI();
    }
  }

}
