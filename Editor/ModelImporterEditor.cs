using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif
using UnityEditorInternal;

namespace Monoid.Unity.Mosaic {

  [CustomEditor(typeof(ModelImporter))]
  public class ModelImporterEditor : ScriptedImporterEditor {

    SerializedProperty addAvatar, addAnimator;
    SerializedProperty optimizeGameObjects;
    SerializedProperty importAnimations;
    SerializedProperty animClipSettings;

    ReorderableList animClipList;

    static class Styles {
      public static readonly GUIContent Title = new GUIContent("Model", "These options control how the model is imported.");

      public static readonly GUIContent AddAvatar = new GUIContent("Add Avatar", "Controls if an avatar for the hierarchy is created.");
      public static readonly GUIContent AddAnimator = new GUIContent("Add Animator", "Controls if an animator is add to the root object.");
      public static readonly GUIContent OptimizeGameObjects = new GUIContent("Optimize Game Objects", "Controls if an animator is add to the root object.");

      public static readonly GUIContent ImportAnimations = new GUIContent("Import Animations", "Controls if animations are imported.");
      public static readonly GUIContent Clips = new GUIContent("Clips", "Animation Clips.");
      public static readonly GUIContent LoopTime = new GUIContent("Loop Time", "Enable to make the animation restart once it ends.");
      public static readonly GUIContent LoopPose = new GUIContent("Loop Pose", "Enable to make the animation loop seamlessly.");
      public static readonly GUIContent CycleOffset = new GUIContent("Cycle Offset", "Offset to the cycle of a looping animation, if we want to start at a different time.");
    }

    public override void OnEnable() {
      base.OnEnable();

      addAvatar = serializedObject.FindProperty("addAvatar");
      addAnimator = serializedObject.FindProperty("addAnimator");
      optimizeGameObjects = serializedObject.FindProperty("optimizeGameObjects");

      importAnimations = serializedObject.FindProperty("importAnimations");
      animClipSettings = serializedObject.FindProperty("animClipSettings");
    }

    public override void OnInspectorGUI() {

      serializedObject.Update();

      GUILayout.Label(Styles.Title, EditorStyles.boldLabel);

      EditorGUILayout.PropertyField(addAvatar, Styles.AddAvatar);
      EditorGUILayout.PropertyField(addAnimator, Styles.AddAnimator);

      if (addAnimator.boolValue && !addAnimator.boolValue) {
        GUI.enabled = addAnimator.boolValue;
        EditorGUILayout.PropertyField(optimizeGameObjects, Styles.OptimizeGameObjects);
        GUI.enabled = true;
      }

      EditorGUILayout.Space();
      EditorGUILayout.PropertyField(importAnimations, Styles.ImportAnimations);

      OnAnimationGUI();

      serializedObject.ApplyModifiedProperties();

      ApplyRevertGUI();
    }

    void OnAnimationGUI() {
      if (!importAnimations.boolValue) {
        return;
      }

      if (animClipList == null) {
        var array = ((ModelImporter)target).animClipSettings;
        var list = new List<ModelImporter.AnimationClipSettings>(array);
        animClipList = new ReorderableList(list, typeof(ModelImporter.AnimationClipSettings), false, true, false, false);
        animClipList.elementHeight = 16;
        animClipList.drawElementCallback = AnimationListElementDraw;
        animClipList.drawHeaderCallback = AnimationListHeaderDraw;
      }
      animClipList.DoLayoutList();

      if (animClipList.index < 0) {
        return;
      }

      var settings = (ModelImporter.AnimationClipSettings)animClipList.list[animClipList.index];

      EditorGUI.BeginChangeCheck();
      EditorGUILayout.LabelField(settings.name, EditorStyles.boldLabel);
      settings.loopTime = EditorGUILayout.Toggle(Styles.LoopTime, settings.loopTime);
      GUI.enabled = settings.loopTime;
      EditorGUI.indentLevel++;
      settings.loopBlend = EditorGUILayout.Toggle(Styles.LoopPose, settings.loopBlend);
      settings.cycleOffset = EditorGUILayout.FloatField(Styles.CycleOffset, settings.cycleOffset);
      EditorGUI.indentLevel--;
      GUI.enabled = true;
      if (EditorGUI.EndChangeCheck()) {
        animClipList.list[animClipList.index] = settings;
        for (int i = 0, n = animClipList.count; i < n; i++) {
          var elem = animClipSettings.GetArrayElementAtIndex(animClipList.index);
          elem.FindPropertyRelative("loopTime").boolValue = settings.loopTime;
          elem.FindPropertyRelative("loopBlend").boolValue = settings.loopBlend;
          elem.FindPropertyRelative("cycleOffset").floatValue = settings.cycleOffset;
        }
      }
    }

    static void AnimationListHeaderDraw(Rect rect) {
      GUI.Label(rect, Styles.Clips, EditorStyles.label);
    }

    void AnimationListElementDraw(Rect rect, int index, bool selected, bool focused) {
      if (animClipList == null) {
        return;
      }
      var settings = (ModelImporter.AnimationClipSettings)animClipList.list[index];
      GUI.Label(rect, settings.name, EditorStyles.label);
    }

    protected override void ResetValues() {
      animClipList = null;
      base.ResetValues();
    }
  }

}
