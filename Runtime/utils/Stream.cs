using System.IO;
using UnityEngine;

namespace Monoid.Unity.Mosaic.Utils {

  public static partial class Stream {

    public static void To(this float[] values, ref float v) {
      v = values.Length > 0 ? values[0] : 0.0f;
    }

    public static void To(this float[] values, ref Vector2 v) {
      v.x = values.Length > 0 ? values[0] : 0.0f;
      v.y = values.Length > 1 ? values[1] : 0.0f;
    }

    public static void To(this float[] values, ref Vector3 v) {
      v.x = values.Length > 0 ? values[0] : 0.0f;
      v.y = values.Length > 1 ? values[1] : 0.0f;
      v.z = values.Length > 2 ? values[2] : 0.0f;
    }

    public static void To(this float[] values, ref Vector4 v) {
      v.x = values.Length > 0 ? values[0] : 0.0f;
      v.y = values.Length > 1 ? values[1] : 0.0f;
      v.z = values.Length > 2 ? values[2] : 0.0f;
      v.w = values.Length > 3 ? values[3] : 1.0f;
    }

    public static void To(this float[] values, ref Matrix4x4 m) {
      m = Matrix4x4.identity;
      for (int i = 0; i < values.Length; i++) {
        m[i / 4, i % 4] = values[i];
      }
    }

    public static void To(this float[] values, ref BoneWeight boneWeight) {
      if (values.Length > 4) {
        float sum = 0.0f;
        for (int i = 0; i < 4; i++) {
          sum += values[i];
        }
        if (sum > 0.0) {
          for (int i = 0; i < 4; i++) {
            values[i] /= sum;
          }
        }
      }

      boneWeight.weight0 = values.Length > 0 ? values[0] : 0.0f;
      boneWeight.weight1 = values.Length > 1 ? values[1] : 0.0f;
      boneWeight.weight2 = values.Length > 2 ? values[2] : 0.0f;
      boneWeight.weight3 = values.Length > 3 ? values[3] : 0.0f;
    }

    public static void To(this int[] values, ref BoneWeight boneWeight) {
      boneWeight.boneIndex0 = values.Length > 0 ? values[0] : 0;
      boneWeight.boneIndex1 = values.Length > 1 ? values[1] : 0;
      boneWeight.boneIndex2 = values.Length > 2 ? values[2] : 0;
      boneWeight.boneIndex3 = values.Length > 3 ? values[3] : 0;
    }

    //-------------------------------------------------------------------------

    public delegate int Int();

    public static Int StreamInt(BinaryReader reader, string type) {
      switch (type.ToLower()) {
        case "byte":
          return () => (int)reader.ReadByte();
        case "short":
          return () => (int)reader.ReadUInt16();
        case "int":
          return () => (int)reader.ReadUInt32();
        case "float":
          return () => (int)reader.ReadSingle();
        default:
          Debug.LogError("Unkown type '" + type + "'");
          return null;
      }
    }

    public delegate void Ints(int[] indices);

    public static Ints StreamInts(BinaryReader reader, string type) {
      switch (type.ToLower()) {
        case "byte":
          return (int[] indices) => {
            for (int i = 0; i < indices.Length; i++) {
              indices[i] = reader.ReadByte();
            }
          };
        case "short":
          return (int[] indices) => {
            for (int i = 0; i < indices.Length; i++) {
              indices[i] = reader.ReadUInt16();
            }
          };
        case "int":
          return (int[] indices) => {
            for (int i = 0; i < indices.Length; i++) {
              indices[i] = (int)reader.ReadUInt32();
            }
          };
        case "float":
          return (int[] indices) => {
            for (int i = 0; i < indices.Length; i++) {
              indices[i] = (int)reader.ReadSingle();
            }
          };
        default:
          Debug.LogError("Unkown type '" + type + "'");
          return null;
      }
    }

    public delegate void Floats(float[] coord);

    public static Floats StreamFloats(BinaryReader reader, string type) {
      switch (type.ToLower()) {
        case "float":
          return (float[] coord) => {
            for (int i = 0; i < coord.Length; i++) {
              coord[i] = reader.ReadSingle();
            }
          };
        case "byte":
          return (float[] coord) => {
            for (int i = 0; i < coord.Length; i++) {
              coord[i] = reader.ReadByte();
            }
          };
        case "short":
          return (float[] coord) => {
            for (int i = 0; i < coord.Length; i++) {
              coord[i] = reader.ReadUInt16();
            }
          };
        case "int":
          return (float[] coord) => {
            for (int i = 0; i < coord.Length; i++) {
              coord[i] = reader.ReadUInt32();
            }
          };
        default:
          Debug.LogError("Unkown type '" + type + "'");
          return null;
      }
    }

    //-------------------------------------------------------------------------

    public static void Read(BinaryReader reader, string type, int size, int count) {
      var stream = StreamInt(reader, type);
      for (int i = 0, n = size * count; i < n; i++) {
        stream();
      }
    }

    public static void Read(BinaryReader reader, string type, int[] indices, int offset = 0) {
      var stream = StreamInt(reader, type);
      for (int i = 0; i < indices.Length; i++) {
        indices[i] = stream() - offset;
      }
    }

    public static void Read(BinaryReader reader, string type, int size, float[] coords) {
      var stream = StreamFloats(reader, type);
      var coord = new float[size];
      for (int i = 0; i < coords.Length; i++) {
        stream(coord);
        coord.To(ref coords[i]);
      }
    }

    public static void Read(BinaryReader reader, string type, int size, Vector2[] coords) {
      var stream = StreamFloats(reader, type);
      var coord = new float[size];
      for (int i = 0; i < coords.Length; i++) {
        stream(coord);
        coord.To(ref coords[i]);
      }
    }

    public static void Read(BinaryReader reader, string type, int size, Vector3[] coords) {
      var stream = StreamFloats(reader, type);
      var coord = new float[size];
      for (int i = 0; i < coords.Length; i++) {
        stream(coord);
        coord.To(ref coords[i]);
      }
    }

    public static void Read(BinaryReader reader, string type, int size, Vector4[] coords) {
      var stream = StreamFloats(reader, type);
      var coord = new float[size];
      for (int i = 0; i < coords.Length; i++) {
        stream(coord);
        coord.To(ref coords[i]);
      }
    }

    public static void Read(BinaryReader reader, string type, int size, Matrix4x4[] matrices) {
      var stream = StreamFloats(reader, type);
      var matrix = new float[size];
      for (int i = 0; i < matrices.Length; i++) {
        stream(matrix);
        matrix.To(ref matrices[i]);
      }
    }

    public static void ReadWeights(BinaryReader reader, string type, int size, BoneWeight[] boneWeights) {
      var stream = StreamFloats(reader, type);
      var coord = new float[size];
      for (int i = 0; i < boneWeights.Length; i++) {
        stream(coord);
        coord.To(ref boneWeights[i]);
      }
    }

    public static void ReadBones(BinaryReader reader, string type, int size, BoneWeight[] boneWeights) {
      var stream = StreamInts(reader, type);
      var indices = new int[size];
      for (int i = 0; i < boneWeights.Length; i++) {
        stream(indices);
        indices.To(ref boneWeights[i]);
      }
    }

  }
}
