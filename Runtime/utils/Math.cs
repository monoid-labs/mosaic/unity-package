using UnityEngine;

namespace Monoid.Unity.Mosaic.Utils {

  public static partial class Math {

    #region Flip

    static readonly Matrix4x4 flip = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(-90, 180, 0), new Vector3(-1, 1, 1));

    public static Vector3 Flip(in Vector3 v) => flip * v;

    public static Vector4 Flip(in Vector4 v) => flip * v;

    public static Vector3[] Flip(params Vector3[] v) {
      for (int i = 0; i < v.Length; i++) {
        v[i] = Flip(in v[i]);
      }
      return v;
    }

    public static Vector4[] Flip(params Vector4[] v) {
      for (int i = 0; i < v.Length; i++) {
        v[i] = Flip(in v[i]);
      }
      return v;
    }

    public static Matrix4x4 Flip(in Matrix4x4 m) => flip * m * flip.inverse;

    public static Matrix4x4[] Flip(params Matrix4x4[] m) {
      for (int i = 0; i < m.Length; i++) {
        m[i] = Flip(in m[i]);
      }
      return m;
    }

    public static int[] Flip(int[] triangles) {
      for (int i = 0, n = triangles.Length - 2; i < n; i += 3) {
        int tmp = triangles[i + 1];
        triangles[i + 1] = triangles[i + 2];
        triangles[i + 2] = tmp;
      }
      return triangles;
    }

    #endregion

    #region Extract

    public static Vector3 ExtractTranslation(in Matrix4x4 m) => m.GetColumn(3);

    public static Quaternion ExtractRotation(in Matrix4x4 m) {
      Vector3 upwards = m.GetColumn(1); //.normalized;
      Vector3 forward = m.GetColumn(2); //.normalized;
      return Quaternion.LookRotation(forward, upwards);
    }

    public static Vector3 ExtractGetScale(in Matrix4x4 m) {
      Vector4 c0 = m.GetColumn(0);
      Vector4 c1 = m.GetColumn(1);
      Vector4 c2 = m.GetColumn(2);

      var s = new Vector3(c0.magnitude, c1.magnitude, c2.magnitude);
      if (Vector3.Dot(Vector3.Cross(c0.normalized, c1.normalized).normalized, ((Vector3)c2).normalized) < 0.99) { // is 0.9 better?
        s.x *= -1;
      }
      return s;
    }

    public static void ExtractTRS(in Matrix4x4 m, out Vector3 t, out Quaternion r, out Vector3 s) {
      t = ExtractTranslation(m);
      r = ExtractRotation(m);
      s = ExtractGetScale(m);
    }

    #endregion

    public static void CalculateW(ref Quaternion q) {
      float w = Mathf.Sqrt(Mathf.Abs(1.0f - q.x * q.x - q.y * q.y - q.z * q.z));
      q.Set(q.x, q.y, q.z, w);
    }

  }
}
