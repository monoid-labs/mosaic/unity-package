using System.IO;
using System.IO.Compression;

namespace Monoid.Unity.Mosaic.Utils {

  public static partial class Zip {

    public static string ReadText(ZipArchiveEntry entry) {
      using (var reader = new StreamReader(entry.Open())) {
        return reader.ReadToEnd();
      }
    }
    public static void WriteText(ZipArchiveEntry entry, string text) {
      using (var file = new StreamWriter(entry.Open())) {
        file.Write(text);
      }
    }

    public static byte[] ReadBinary(ZipArchiveEntry entry) {
      using (var stream = entry.Open()) {
        var buffer = new byte[1024];
        using (var memory = new MemoryStream()) {
          int read;
          while ((read = stream.Read(buffer, 0, buffer.Length)) > 0) {
            memory.Write(buffer, 0, read);
          }
          return memory.ToArray();
        }
      }
    }

    public static ZipArchive CreateZip(string path) {
      return CreateZip(new FileStream(path, FileMode.Create));
    }

    public static ZipArchive CreateZip(System.IO.Stream stream) {
      return new ZipArchive(stream, ZipArchiveMode.Create, false);
    }

  }

}
