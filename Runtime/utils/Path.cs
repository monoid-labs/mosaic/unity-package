using System;

namespace Monoid.Unity.Mosaic.Utils {

  public static partial class Path {

    public static string Resolve(string path, string wd = "") {
      if (System.IO.Path.DirectorySeparatorChar != '/') {
        path = path.Replace(System.IO.Path.DirectorySeparatorChar, '/');
      }
      if (path.Length > 0 && path[0] == '/') {
        return path;
      }
      if (wd.Length > 0 && wd[wd.Length - 1] != '/') {
        wd += '/';
      }
      return Simplify(wd + path);
    }
    public static string Simplify(string path) {
      return DotSlash(SlashDotDotSlash(SlashSlash(path)));
    }
    static string SlashSlash(string path) {
      string last;
      do {
        last = path;
        path = path.Replace("//", "/");
      } while (path != last);
      return path;
    }
    static string SlashDotDotSlash(string path) {
      int split, prev;
      while ((split = path.IndexOf("/../")) > 0) {
        prev = path.LastIndexOf("/", split - 1);
        if (prev < 0) {
          return path;
        }
        path = path.Substring(0, prev) + path.Substring(split + 3);
      }
      return path;
    }
    static string DotSlash(string path) {
      return path.Replace("./", "");
    }

    public static string Relative(string path, string dir = "") {
      path = System.IO.Path.GetFullPath(path);
      dir = System.IO.Path.GetFullPath(dir);
      if (!dir.EndsWith("/")) {
        dir += "/";
      }
      var pathUri = new Uri(path);
      var dirUri = new Uri(dir);
      var relative = Uri.UnescapeDataString(dirUri.MakeRelativeUri(pathUri).ToString().Replace(System.IO.Path.DirectorySeparatorChar, '/'));
      return relative;
    }

  }
}
