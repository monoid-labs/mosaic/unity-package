using System;

namespace Monoid.Unity.Mosaic {

  [Serializable]
  public class Material {

    const string DefaultShader = "Standard";

    [Serializable]
    public struct Texture {
      public string name;
      public string image;
    }

    [Serializable]
    public struct Constant {
      public string name;
      public float[] value;
    }

    public string shader;
    public Texture[] textures = { };
    public Constant[] constants = { };

    // static string FindProperty(UnityEngine.Shader shader, string name) {
    // 	name = name.ToLower();
    // 	for(int i=0, n=ShaderUtil.GetPropertyCount(shader); i<n; i++) {
    // 		var prop = ShaderUtil.GetPropertyName(shader, i);
    // 		Debug.Log(prop + " !");
    // 		if (name == prop.ToLower()) {
    // 			return prop;
    // 		}
    // 	}
    // 	return string.Empty;
    // }

  }
}
