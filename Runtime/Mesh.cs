using System;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;

namespace Monoid.Unity.Mosaic {

  [Serializable]
  public class Mesh {

    [Serializable]
    public class Coordinates {
      public int count;
      public Attribute[] attributes = { };
    }

    [Serializable]
    public class Indices {
      public string type;
      public IndexGroup[] groups = { };
    }

    public Coordinates vertices = new Coordinates();
    public Indices indices = new Indices();

    public bool Load(byte[] bytes, UnityEngine.Mesh mesh, Generate genNormals, Generate genTangents) {
      var positions = new Vector3[vertices.count];
      Vector3[] normals = null;
      Vector4[] tangents = null;
      var texcoords = new Vector2[0][];
      var triangles = new int[0][];

      using (var reader = new BinaryReader(new MemoryStream(bytes))) {

        for (int a = 0; a < vertices.attributes.Length; a++) {
          vertices.attributes[a].frames = Math.Max(1, vertices.attributes[a].frames);
          var attribute = vertices.attributes[a];
          //Debug.Log("vertex." + attribute.name + "[type="+attribute.type+",frames="+attribute.frames+"]");

          switch (attribute.name.ToLower()) {
            case "position":
              if (attribute.frames > 1) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has '" + attribute.size + "' vertex position frames but only 1 is supported");
              }
              if (attribute.size > 3) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has vertex positions with dimension '" + attribute.size + "' but only up to 3 is supported in Unity");
              }
              Utils.Stream.Read(reader, attribute.type, attribute.size, positions);
              Utils.Math.Flip(positions);
              Utils.Stream.Read(reader, attribute.type, attribute.size, vertices.count * (attribute.frames - 1)); // skip
              break;
            case "normal":
              if (attribute.frames > 1) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has '" + attribute.size + "' vertex normal frames but only 1 is supported");
              }
              if (attribute.size > 3) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has normals with dimension '" + attribute.size + "' but only up to 3 is supported in Unity");
              }
              normals = new Vector3[vertices.count];
              Utils.Stream.Read(reader, attribute.type, attribute.size, normals);
              Utils.Math.Flip(normals);
              Utils.Stream.Read(reader, attribute.type, attribute.size, vertices.count * (attribute.frames - 1)); // skip
              break;
            case "texcoord":
            case "texture":
              if (attribute.size > 2) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has texture coordinates with dimension '" + attribute.size + "' but only up to 2 is supported in Unity");
              }
              if (attribute.frames > 4) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has texture coordinates with frames '" + attribute.frames + "' but only up to 4 uv sets are supported in Unity");
              }
              texcoords = new Vector2[attribute.frames][];
              for (int f = 0; f < texcoords.Length; f++) {
                texcoords[f] = new Vector2[vertices.count];
                Utils.Stream.Read(reader, attribute.type, attribute.size, texcoords[f]);
              }
              break;
            case "tangent":
              if (attribute.frames > 1) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has '" + attribute.size + "' vertex tangents frames but only 1 is supported");
              }
              if (attribute.size > 4) {
                Debug.LogWarning("Mesh(" + mesh.name + ") has tangents with dimension '" + attribute.size + "' but only up to 4 is supported in Unity");
              }
              tangents = new Vector4[vertices.count];
              Utils.Stream.Read(reader, attribute.type, attribute.size, tangents);
              Utils.Math.Flip(tangents);
              Utils.Stream.Read(reader, attribute.type, attribute.size, vertices.count * (attribute.frames - 1)); // skip
              break;
            default:
              // skip
              Utils.Stream.Read(reader, attribute.type, attribute.size, attribute.frames * vertices.count);
              Debug.LogWarning("Mesh(" + mesh.name + ") has unsupported vertex attribute '" + attribute.name + "'");
              break;
          }
        }

        triangles = new int[indices.groups.Length][];
        for (int g = 0; g < triangles.Length; g++) {
          var group = indices.groups[g];
          triangles[g] = new int[group.count];
          // if (group.type.ToLower() == "range") {
          //     for (int i=0; i<group.count; i++) {
          //         triangles[g][i] = group.offset + i;
          //     }
          // } else {
          Utils.Stream.Read(reader, indices.type, triangles[g], group.offset);
          Utils.Math.Flip(triangles[g]);
          // }
        }
      }

      mesh.vertices = positions;

      mesh.indexFormat = indices.type.ToLower() == "int" ? IndexFormat.UInt32 : IndexFormat.UInt16;
      mesh.subMeshCount = triangles.Length;
      for (int i = 0; i < triangles.Length; i++) {
        mesh.SetIndices(triangles[i], MeshTopology.Triangles, i);
      }

      switch (genNormals) {
        case Generate.Import:
          if (normals != null) {
            mesh.normals = normals;
          }
          break;
        case Generate.FillIn:
          if (normals != null) {
            mesh.normals = normals;
          } else {
            mesh.RecalculateNormals();
          }
          break;
        case Generate.Replace:
          mesh.RecalculateNormals();
          break;
      }

      if (texcoords.Length > 0) {
        mesh.uv = texcoords[0];
        if (texcoords.Length > 1) {
          mesh.uv2 = texcoords[1];
        }
        if (texcoords.Length > 2) {
          mesh.uv3 = texcoords[2];
        }
        if (texcoords.Length > 3) {
          mesh.uv4 = texcoords[3];
        }
      }

      switch (genTangents) {
        case Generate.Import:
          if (tangents != null) {
            mesh.tangents = tangents;
          }
          break;
        case Generate.FillIn:
          if (tangents != null) {
            mesh.tangents = tangents;
          } else {
            mesh.RecalculateTangents();
          }
          break;
        case Generate.Replace:
          mesh.RecalculateTangents();
          break;
      }

      mesh.RecalculateBounds();
      return true;
    }

    public string[] MaterialPaths(string dir = "") {
      var materials = new string[indices.groups.Length];
      for (int i = 0; i < materials.Length; i++) {
        var id = indices.groups[i].name;
        if (string.IsNullOrEmpty(id)) {
          id = "Default";//string.Empty;
        }
        string name, path;
        Names.Instance(id, out name, out path);
        materials[i] = Utils.Path.Resolve(path, dir);
      }
      return materials;
    }
  }
}
