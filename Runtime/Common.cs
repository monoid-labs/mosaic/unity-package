using System;

namespace Monoid.Unity.Mosaic {

  [Serializable]
  public struct Attribute {
    public string name;
    public string type;
    public int size;
    public int frames;
  }

  [Serializable]
  public struct IndexGroup {
    public string name;
    public int count;
    public int offset;
    public string type;
  }

}
