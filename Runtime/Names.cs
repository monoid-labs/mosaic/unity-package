using System.IO;

namespace Monoid.Unity.Mosaic {

  public static partial class Names {
    public static void Instance(string id, out string name, out string path) {
      var split = id.Split('@');
      if (split.Length > 1) {
        name = split[0];
        path = split[1];
      } else {
        name = Path.GetFileName(id);
        path = id;
      }
    }
  }
}
