using System;

namespace Monoid.Unity.Mosaic {

  [Serializable]
  public class Model {

    [Serializable]
    public class Bone {
      public string name;
      public int parent;
      public Transform3D transform;
    }

    [Serializable]
    public class State {
      public string name;
      public float value;
    }

    [Serializable]
    public class Shape {
      public string name;
      public int bone;
    }

    public Bone[] bones = { };
    public State[] states = { };
    public Shape[] shapes = { };
  }
}
