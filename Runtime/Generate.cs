namespace Monoid.Unity.Mosaic {

  public enum Generate {
    Clear, Import, FillIn, Replace
  }

}
