using System.Collections.Generic;
using UnityEngine;

namespace Monoid.Unity.Mosaic {
  public abstract class ShaderProperties {

    //-------------------------------------------------------------------------

    static readonly Dictionary<string, ShaderProperties> map = new Dictionary<string, ShaderProperties>();

    public static ShaderProperties Get(string shader) {
      shader = shader.ToLower();
      ShaderProperties properties;
      map.TryGetValue(shader, out properties);
      return properties;
    }

    public static void Set(string shader, ShaderProperties properties) {
      shader = shader.ToLower();
      map[shader] = properties;
    }

    //-------------------------------------------------------------------------

    public abstract bool Set(UnityEngine.Material mat, string name, Texture texture);

    public abstract bool Set(UnityEngine.Material mat, string name, Vector4 constant);

    //-------------------------------------------------------------------------

    public class Standard : ShaderProperties {

      public override bool Set(UnityEngine.Material mat, string name, Texture texture) {
        name = name.ToLower();
        switch (name) {
          case "albedo":
            mat.SetTexture("_MainTex", texture);
            break;
          case "normal":
            mat.SetTexture("_BumpMap", texture);
            mat.EnableKeyword("_NORMALMAP");
            break;
          case "emission":
            mat.SetTexture("_EmissionMap", texture);
            if (mat.globalIlluminationFlags == MaterialGlobalIlluminationFlags.EmissiveIsBlack) {
              mat.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
            }
            break;
          default:
            return false;
        }
        return true;
      }

      public override bool Set(UnityEngine.Material mat, string name, Vector4 constant) {
        name = name.ToLower();
        switch (name) {
          case "albedo":
            mat.SetVector("_Color", constant);
            break;
          case "normal":
            mat.SetVector("_BumpScale", constant);
            break;
          case "emission":
            mat.SetVector("_EmissionColor", constant);
            if (mat.globalIlluminationFlags == MaterialGlobalIlluminationFlags.EmissiveIsBlack) {
              mat.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
            }
            break;
          default:
            return false;
        }
        return true;
      }
    }

    //-------------------------------------------------------------------------
  }
}
