using System;
using System.IO;
using UnityEngine;

namespace Monoid.Unity.Mosaic {

  [Serializable]
  public class Modifier {

    [Serializable]
    public class Blends {
      public Attribute[] attributes = { };
    }
    [Serializable]
    public class Targets {
      public string type;
      public IndexGroup[] groups = { };
    }

    public Blends blends = new Blends();
    public Targets targets = new Targets();

    //-------------------------------------------------------------------------

    public string[] Names() {
      var names = new string[targets.groups.Length];
      for (int i = 0; i < names.Length; i++) {
        names[i] = targets.groups[i].name;
      }
      return names;
    }

    //-------------------------------------------------------------------------

    public UnityEngine.Transform[] FindBoneTransforms(UnityEngine.Transform skeleton) {
      var skeletonBones = skeleton.GetComponentsInChildren<UnityEngine.Transform>(true);
      var bones = new UnityEngine.Transform[targets.groups.Length];
      for (int i = 0; i < bones.Length; i++) {
        var name = targets.groups[i].name;
        for (int j = 0; j < skeletonBones.Length; j++) {
          if (skeletonBones[j].name == name) {
            bones[i] = skeletonBones[j];
            break;
          }
        }
        if (bones[i] == null) {
          Debug.LogWarning("Cannot find bone '" + name + "'", skeleton);
        }
      }
      return bones;
    }

    public bool LoadSkin(byte[] bytes, UnityEngine.Mesh mesh) {
      Matrix4x4[] bindposes = null;
      float[] weights = null;
      var indices = new int[targets.groups.Length][];

      using (var reader = new BinaryReader(new MemoryStream(bytes))) {
        for (int a = 0; a < blends.attributes.Length; a++) {
          var attr = blends.attributes[a];
          switch (attr.name.ToLower()) {
            case "bind":
              if (attr.size != 16) {
                Debug.LogWarning("Skin(" + mesh.name + ") has property bind with dimension '" + attr.size + "' but expected 16");
              }
              if (bindposes == null) {
                bindposes = new Matrix4x4[attr.frames];
              }
              Utils.Stream.Read(reader, attr.type, attr.size, bindposes);
              Utils.Math.Flip(bindposes);
              break;
            case "weight":
            case "weights":
              if (attr.size != 1) {
                Debug.LogWarning("Skin(" + mesh.name + ") has property bind with dimension '" + attr.size + "' but expected 1");
              }
              if (weights == null) {
                weights = new float[attr.frames];
              }
              Utils.Stream.Read(reader, attr.type, attr.size, weights);
              break;
            default:
              // skip
              Utils.Stream.Read(reader, attr.type, attr.size, attr.frames);
              Debug.LogWarning("Skin(" + mesh.name + ") has unsupported blend attribute '" + attr.name + "'");
              break;
          }
        }

        for (int g = 0; g < targets.groups.Length; g++) {
          var grp = targets.groups[g];
          indices[g] = new int[grp.count];
          Utils.Stream.Read(reader, targets.type, indices[g]);
        }
      }

      if (weights == null) {
        Debug.LogError("Skin(" + mesh.name + "): has no weights");
        return false;
      }

      var boneWeights = new BoneWeight[mesh.vertexCount];
      for (int bone = 0, off = 0; bone < indices.Length; bone++) {
        var boneTargets = indices[bone];
        for (int j = 0; j < boneTargets.Length; j++) {
          int vertex = boneTargets[j];
          float weight = weights[off + j];
          var limit = boneWeights[vertex].weight3 > 0;
          AddBoneWeight(ref boneWeights[vertex], weight, bone);
          if (limit) {
            Debug.LogWarning("Skin(" + mesh.name + "): vertex[" + vertex + "] has more than 4 bone weights.");
          }
        }
        off += boneTargets.Length;
      }

      if (bindposes != null) {
        mesh.bindposes = bindposes;
      }
      mesh.boneWeights = boneWeights;
      return true;
    }

    public UnityEngine.Transform[] LoadSkin(byte[] bytes, UnityEngine.Mesh mesh, UnityEngine.Transform skeleton) {
      LoadSkin(bytes, mesh);

      var bones = FindBoneTransforms(skeleton);

      // assume skeleton is in bind pose if skin has none set
      if (mesh.bindposes.Length == 0) {
        var bindposes = new Matrix4x4[targets.groups.Length];
        for (int i = 0; i < bindposes.Length; i++) {
          if (bones[i]) {
            bindposes[i] = bones[i].worldToLocalMatrix * skeleton.localToWorldMatrix;
          }
        }
        mesh.bindposes = bindposes;
      }

      return bones;
    }

    static void AddBoneWeight(ref BoneWeight boneWeight, float weight, int bone) {
      if (weight > boneWeight.weight0) {
        boneWeight.weight3 = boneWeight.weight2;
        boneWeight.boneIndex3 = boneWeight.boneIndex2;
        boneWeight.weight2 = boneWeight.weight1;
        boneWeight.boneIndex2 = boneWeight.boneIndex1;
        boneWeight.weight1 = boneWeight.weight0;
        boneWeight.boneIndex1 = boneWeight.boneIndex0;
        boneWeight.weight0 = weight;
        boneWeight.boneIndex0 = bone;
      } else if (weight > boneWeight.weight1) {
        boneWeight.weight3 = boneWeight.weight2;
        boneWeight.boneIndex3 = boneWeight.boneIndex2;
        boneWeight.weight2 = boneWeight.weight1;
        boneWeight.boneIndex2 = boneWeight.boneIndex1;
        boneWeight.weight1 = weight;
        boneWeight.boneIndex1 = bone;
      } else if (weight > boneWeight.weight2) {
        boneWeight.weight3 = boneWeight.weight2;
        boneWeight.boneIndex3 = boneWeight.boneIndex2;
        boneWeight.weight2 = weight;
        boneWeight.boneIndex2 = bone;
      } else if (weight > boneWeight.weight3) {
        boneWeight.weight3 = weight;
        boneWeight.boneIndex3 = bone;
      }
    }

    //-------------------------------------------------------------------------

    public bool LoadMorph(byte[] bytes, UnityEngine.Mesh mesh, Generate genNormals, Generate genTangents) {
      int count = targets.groups.Length;

      Vector3[] positions = null, normals = null, tangents = null;
      int[][] indices = new int[count][];

      using (var reader = new BinaryReader(new MemoryStream(bytes))) {

        for (int a = 0; a < blends.attributes.Length; a++) {
          var attribute = blends.attributes[a];
          // Debug.Log("blend." + attribute.name + "[type="+attribute.type+",frames="+attribute.frames+"]");
          switch (attribute.name.ToLower()) {
            case "position":
              if (attribute.size > 3) {
                Debug.LogWarning("Morph(" + mesh.name + ") has blend positions with dimension '" + attribute.size + "' but only up to 3 is supported in Unity");
              }
              positions = new Vector3[attribute.frames];
              Utils.Stream.Read(reader, attribute.type, attribute.size, positions);
              Utils.Math.Flip(positions);
              break;
            case "normal":
              if (attribute.size > 3) {
                Debug.LogWarning("Morph(" + mesh.name + ") has blend normals with dimension '" + attribute.size + "' but only up to 3 is supported in Unity");
              }
              normals = new Vector3[attribute.frames];
              Utils.Stream.Read(reader, attribute.type, attribute.size, normals);
              Utils.Math.Flip(normals);
              break;
            case "tangent":
              if (attribute.size > 3) {
                Debug.LogWarning("Morph(" + mesh.name + ") has blend tangets with dimension '" + attribute.size + "' but only up to 3 is supported in Unity");
              }
              tangents = new Vector3[attribute.frames];
              Utils.Stream.Read(reader, attribute.type, attribute.size, tangents);
              Utils.Math.Flip(tangents);
              break;
            default:
              // skip
              Utils.Stream.Read(reader, attribute.type, attribute.size, attribute.frames);
              Debug.LogWarning("Morph(" + mesh.name + ") has unsupported blend attribute '" + attribute.name + "'");
              break;
          }
        }

        for (int g = 0; g < targets.groups.Length; g++) {
          var grp = targets.groups[g];
          indices[g] = new int[grp.count];
          Utils.Stream.Read(reader, targets.type, indices[g]);
        }
      }

      int vertices = mesh.vertexCount;

      int off = 0;
      for (int i = 0; i < count; i++) {
        var p = positions != null ? new Vector3[vertices] : null;
        var n = normals != null ? new Vector3[vertices] : null;
        var t = tangents != null ? new Vector3[vertices] : null;

        for (int j = 0; j < indices[i].Length; j++) {
          int v = indices[i][j];
          if (p != null) {
            p[v] = positions[off + j];
          }
          if (n != null) {
            n[v] = normals[off + j];
          }
          if (t != null) {
            t[v] = tangents[off + j];
          }
        }
        off += indices[i].Length;

        if (p == null) {
          Debug.LogWarning("Mosaic-Mesh: Skipping morph-target without positions: " + targets.groups[i].name);
          continue;
        }

        bool recalcNormals = false;
        switch (genNormals) {
          case Generate.Clear:
            n = null;
            break;
          case Generate.FillIn:
            recalcNormals = n == null;
            break;
          case Generate.Replace:
            recalcNormals = true;
            break;
        }
        bool recalcTangents = false;
        switch (genTangents) {
          case Generate.Clear:
            t = null;
            break;
          case Generate.FillIn:
            recalcTangents = t == null;
            break;
          case Generate.Replace:
            recalcTangents = true;
            break;
        }

        Recalculate(mesh, p, ref n, ref t, recalcNormals, recalcTangents);

        mesh.AddBlendShapeFrame(targets.groups[i].name, 1.0f, p, n, t);
      }
      return true;
    }

    static void Recalculate(UnityEngine.Mesh mesh, Vector3[] deltaPositions, ref Vector3[] deltaNormals, ref Vector3[] deltaTangents, bool rcNormals, bool rcTangents) {
      if (!rcNormals && !rcTangents) {
        return;
      }

      var positions = mesh.vertices;
      var normals = mesh.normals;
      var tangents = mesh.tangents;

      for (int i = 0; i < positions.Length; i++) {
        positions[i] += deltaPositions[i];
      }
      if (deltaNormals != null) {
        for (int i = 0; i < normals.Length; i++) {
          normals[i] += deltaNormals[i];
        }
      }
      if (deltaTangents != null) {
        for (int i = 0; i < tangents.Length; i++) {
          tangents[i] += new Vector4(deltaTangents[i].x, deltaTangents[i].y, deltaTangents[i].z, 0.0f);
        }
      }

      var tmp = new UnityEngine.Mesh();
      tmp.vertices = positions;
      tmp.normals = normals;
      tmp.uv = mesh.uv;
      tmp.tangents = tangents;
      tmp.triangles = mesh.triangles;

      if (rcNormals && normals.Length > 0) {
        tmp.RecalculateNormals();
        var blendNormals = tmp.normals;
        deltaNormals = new Vector3[blendNormals.Length];
        for (int i = 0; i < deltaNormals.Length; i++) {
          deltaNormals[i] = blendNormals[i] - normals[i];
        }
      }
      if (rcTangents && tangents.Length > 0) {
        tmp.RecalculateTangents();
        var blendTangents = tmp.tangents;
        deltaTangents = new Vector3[blendTangents.Length];
        for (int i = 0; i < deltaTangents.Length; i++) {
          deltaTangents[i] = blendTangents[i] - tangents[i];
        }
      }

      UnityEngine.Object.DestroyImmediate(tmp);
    }

    //-------------------------------------------------------------------------
  }
}
