using System;
using UnityEngine;

namespace Monoid.Unity.Mosaic {

  [Serializable]
  public struct Transform3D {

    public float[] translate;
    public float[] rotate;
    public float[] scale;

    public void Set(UnityEngine.Transform transform) {
      var t = Vector3.zero;
      var r = Quaternion.identity;
      var s = Vector3.one;
      if (translate != null) {
        for (int i = 0, n = System.Math.Min(3, translate.Length); i < n; i++) {
          t[i] = translate[i];
        }
      }
      if (rotate != null) {
        for (int i = 0, n = System.Math.Min(4, rotate.Length); i < n; i++) {
          r[i] = rotate[i];
        }
        Utils.Math.CalculateW(ref r);
      }
      if (scale != null) {
        for (int i = 0, n = System.Math.Min(3, scale.Length); i < n; i++) {
          s[i] = scale[i];
        }
      }

      var m = Matrix4x4.TRS(t, r, s);
      m = Utils.Math.Flip(m);
      Utils.Math.ExtractTRS(in m, out t, out r, out s);

      transform.localPosition = t;
      transform.localRotation = r;
      transform.localScale = s;
    }

    public Matrix4x4 Matrix() {
      var t = Vector3.zero;
      var r = Quaternion.identity;
      var s = Vector3.one;
      if (translate != null) {
        for (int i = 0, n = System.Math.Min(3, translate.Length); i < n; i++) {
          t[i] = translate[i];
        }
      }
      if (rotate != null) {
        for (int i = 0, n = System.Math.Min(4, rotate.Length); i < n; i++) {
          r[i] = rotate[i];
        }
        Utils.Math.CalculateW(ref r);
      }
      if (scale != null) {
        for (int i = 0, n = System.Math.Min(3, scale.Length); i < n; i++) {
          s[i] = scale[i];
        }
      }

      return Matrix4x4.TRS(t, r, s);
    }
  }
}
