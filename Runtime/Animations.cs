using System;
using System.IO;
using UnityEngine;

namespace Monoid.Unity.Mosaic {

  [Serializable]
  public class Animations {

    [Serializable]
    public class Action {
      public string name;
      public float from = 0.0f;
      public float to = 1.0f;
      public bool loop = false;
    }

    [Serializable]
    public class Channel {
      public string name;
      public float from = 0.0f;
      public float to = 1.0f;
      public Attribute[] attributes = { };
    }

    public float duration;
    public Action[] actions = { };
    public Channel[] channels = { };

    public AnimationClip Load(string name, byte[] bytes, UnityEngine.Transform target) {

      var keys = LoadKeys(bytes);

      var clip = new AnimationClip {
        name = name
      };

      for (int c = 0; c < channels.Length; c++) {
        var channel = channels[c];
        UnityEngine.Transform t = FindDescendant(target, channel.name);
        if (t != null) {
          var path = FindPath(t, target);
          //Debug.Log(path);
          AddTransform(clip, path,
          Matrix4x4.TRS(t.localPosition, t.localRotation, t.localScale),
          channel, duration, keys[c], 0, 1);
        }
      }

      clip.wrapMode = WrapMode.Loop;
      clip.EnsureQuaternionContinuity();
      return clip;
    }

    public AnimationClip[] LoadActions(string name, byte[] bytes, UnityEngine.Transform target) {
      if (actions.Length == 0) {
        var clip = this.Load(name, bytes, target);
        return clip != null ? new AnimationClip[] { clip } : new AnimationClip[0];
      }

      var keys = LoadKeys(bytes);

      var clips = new UnityEngine.AnimationClip[actions.Length];

      for (int i = 0; i < actions.Length; i++) {
        var action = actions[i];
        var clip = clips[i] = new UnityEngine.AnimationClip();
        clip.name = name + "." + action.name;

        float from = Mathf.Clamp01(action.from);
        float to = Mathf.Clamp(action.to, from, 1);

        for (int c = 0; c < channels.Length; c++) {
          var channel = channels[c];
          UnityEngine.Transform t = FindDescendant(target, channel.name);
          if (t != null) {
            var path = FindPath(t, target);
            //Debug.Log(path);
            AddTransform(clip, path,
                         Matrix4x4.TRS(t.localPosition, t.localRotation, t.localScale),
                         channel, duration, keys[c], from, to);
          }
        }

        clip.wrapMode = WrapMode.Loop;
        clip.EnsureQuaternionContinuity();
      }
      return clips;
    }

    float[][][] LoadKeys(byte[] bytes) {
      var keys = new float[channels.Length][][];

      using (var reader = new BinaryReader(new MemoryStream(bytes))) {
        for (int a = 0; a < keys.Length; a++) {
          var channel = channels[a];
          keys[a] = new float[channel.attributes.Length][];
          for (int c = 0; c < keys[a].Length; c++) {
            var attribute = channel.attributes[c];
            var frames = keys[a][c] = new float[attribute.size * attribute.frames];
            for (int k = 0; k < frames.Length; k++) {
              frames[k] = reader.ReadSingle();
            }
          }
        }
      }
      return keys;
    }

    void AddTransform(AnimationClip clip, string name, Matrix4x4 matrix, Channel channel, float duration, float[][] keys, float from, float to) {
      int t = -1, r = -1, s = -1;
      int samples = 0;
      for (int i = 0; i < channel.attributes.Length; i++) {
        var attribute = channel.attributes[i];
        switch (attribute.name.ToLower()) {
          case "translate":
            t = i;
            samples = Math.Max(samples, attribute.frames);
            break;
          case "rotate":
            r = i;
            samples = Math.Max(samples, attribute.frames);
            break;
          case "scale":
            s = i;
            samples = Math.Max(samples, attribute.frames);
            break;
        }
      }
      if (t < 0 && r < 0 && s < 0) {
        return;
      }

      samples = Mathf.CeilToInt(samples * (to - from) / (channel.to - channel.from));

      //TODO(micha): don't sample but use exact keys
      var translateSamples = new Vector3[samples];
      var rotateSamples = new Quaternion[samples];
      var scaleSamples = new Vector3[samples];

      for (int i = 0; i < samples; i++) {
        float time = from + (i / (float)(samples - 1)) * (to - from);
        time = Warp(time, channel.from, channel.to);
        time = Mathf.Clamp01(time);

        var translate = Vector3.zero;
        var rotate = Quaternion.identity;
        var scale = Vector3.one;

        int pre, post;
        if (t >= 0) {
          var attribute = channel.attributes[t];
          float progress = SampleKeys(time, attribute.frames, out pre, out post);
          translate = SampleTranslate(progress, pre, post, keys[t], attribute.size);
        }
        if (r >= 0) {
          var attribute = channel.attributes[r];
          float progress = SampleKeys(time, attribute.frames, out pre, out post);
          rotate = SampleRotate(progress, pre, post, keys[r], attribute.size);
        }
        if (s >= 0) {
          var attribute = channel.attributes[s];
          float progress = SampleKeys(time, attribute.frames, out pre, out post);
          scale = SampleScale(progress, pre, post, keys[s], attribute.size);
        }

        var trs = matrix * Utils.Math.Flip(Matrix4x4.TRS(translate, rotate, scale));
        Utils.Math.ExtractTRS(in trs, out translate, out rotate, out scale);

        translateSamples[i] = translate;
        rotateSamples[i] = rotate;
        scaleSamples[i] = scale;
      }

      duration *= (to - from);

      var translateCurves = AnimationCurves(KeyFrames(duration, translateSamples));
      var rotateCurves = AnimationCurves(KeyFrames(duration, rotateSamples));
      var scaleCurves = AnimationCurves(KeyFrames(duration, scaleSamples));

      var type = typeof(UnityEngine.Transform);
      clip.SetCurve(name, type, "localPosition.x", translateCurves[0]);
      clip.SetCurve(name, type, "localPosition.y", translateCurves[1]);
      clip.SetCurve(name, type, "localPosition.z", translateCurves[2]);

      clip.SetCurve(name, type, "localRotation.x", rotateCurves[0]);
      clip.SetCurve(name, type, "localRotation.y", rotateCurves[1]);
      clip.SetCurve(name, type, "localRotation.z", rotateCurves[2]);
      clip.SetCurve(name, type, "localRotation.w", rotateCurves[3]);

      clip.SetCurve(name, type, "localScale.x", scaleCurves[0]);
      clip.SetCurve(name, type, "localScale.y", scaleCurves[1]);
      clip.SetCurve(name, type, "localScale.z", scaleCurves[2]);
    }

    static AnimationCurve[] AnimationCurves(Keyframe[][] keyframes) {
      var curves = new AnimationCurve[keyframes.Length];
      for (int i = 0; i < curves.Length; i++) {
        curves[i] = new AnimationCurve(keyframes[i]) {
          preWrapMode = WrapMode.ClampForever,
          postWrapMode = WrapMode.ClampForever
        };
      }
      return curves;
    }

    static Keyframe[][] KeyFrames(float duration, Vector3[] samples) {
      var keyframes = new Keyframe[3][];
      keyframes[0] = new Keyframe[samples.Length];
      keyframes[1] = new Keyframe[samples.Length];
      keyframes[2] = new Keyframe[samples.Length];
      for (int i = 0, n = samples.Length; i < n; i++) {
        float time = (i / (float)(n - 1)) * duration;
        keyframes[0][i] = new Keyframe(time, samples[i].x);
        keyframes[1][i] = new Keyframe(time, samples[i].y);
        keyframes[2][i] = new Keyframe(time, samples[i].z);
      }
      return keyframes;
    }
    static Keyframe[][] KeyFrames(float duration, Quaternion[] samples) {
      var keyframes = new Keyframe[4][];
      keyframes[0] = new Keyframe[samples.Length];
      keyframes[1] = new Keyframe[samples.Length];
      keyframes[2] = new Keyframe[samples.Length];
      keyframes[3] = new Keyframe[samples.Length];
      for (int i = 0, n = samples.Length; i < n; i++) {
        float time = (i / (float)(n - 1)) * duration;
        keyframes[0][i] = new Keyframe(time, samples[i].x);
        keyframes[1][i] = new Keyframe(time, samples[i].y);
        keyframes[2][i] = new Keyframe(time, samples[i].z);
        keyframes[3][i] = new Keyframe(time, samples[i].w);
      }
      return keyframes;
    }

    static float Warp(float progress, float from, float to) {
      progress -= from;
      progress /= to - from;
      return progress;
    }

    static float SampleKeys(float progress, int frames, out int pre, out int post) {
      frames--;
      progress *= frames;
      pre = Mathf.FloorToInt(progress);
      post = Mathf.CeilToInt(progress);
      progress -= pre;

      pre = Math.Max(0, Math.Min(pre, frames));
      post = Math.Max(0, Math.Min(post, frames));
      progress = Mathf.Clamp01(progress);
      return progress;
    }

    static Vector3 SampleTranslate(float progress, int pre, int post, float[] keys, int size) {
      var sample = Vector4.zero;
      pre *= size;
      post *= size;
      for (int i = 0; i < size; i++) {
        float a = keys[pre + i];
        float b = keys[post + i];
        sample[i] = a * (1 - progress) + b * progress;
      }
      return sample;
    }
    static Quaternion SampleRotate(float progress, int pre, int post, float[] keys, int size) {
      var sample = Quaternion.identity;
      pre *= size;
      post *= size;
      for (int i = 0; i < size; i++) {
        float a = keys[pre + i];
        float b = keys[post + i];
        sample[i] = a * (1 - progress) + b * progress;
      }
      Utils.Math.CalculateW(ref sample);
      return sample;
    }
    static Vector3 SampleScale(float progress, int pre, int post, float[] keys, int size) {
      var sample = Vector4.one;
      pre *= size;
      post *= size;
      for (int i = 0; i < size; i++) {
        float a = keys[pre + i];
        float b = keys[post + i];
        sample[i] = a * (1 - progress) + b * progress;
      }
      return sample;
    }

    static UnityEngine.Transform FindDescendant(UnityEngine.Transform t, string name) {
      if (t.name == name) {
        return t;
      }
      foreach (UnityEngine.Transform child in t) {
        var result = FindDescendant(child, name);
        if (result != null) {
          return result;
        }
      }
      return null;
    }

    static string FindPath(UnityEngine.Transform c, UnityEngine.Transform p) {
      var name = c.name;
      if (c.parent != null && c.parent != p) {
        return FindPath(c.parent, p) + "/" + name;
      }
      return name;
    }
  }
}
